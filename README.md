Platinum Communication Challenge Task
=

> Sure, I can provide you with the steps to set up a Laravel project after cloning it from GitHub and then running it locally. Below is a step-by-step guide in Markdown format:
##### Setting Up a Cloned Laravel Project Locally


###### Clone the Repository:

* `git clone https://gitlab.com/mudassaralichouhan/platinum-communication-challenge-task.git`

###### Navigate to the Project Directory:

* `cd platinum-communication-challenge-task`

###### Install Composer Dependencies:

* `composer install`

###### Create .env File:

* `cp .env.example .env`


###### Generate Application Key:

* `php artisan key:generate`

###### Set Up Database:

* Create a new database in your local database management tool.
* Update `.env` file with your database credentials:

        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=platinum_task
        DB_USERNAME=root
        DB_PASSWORD=


###### Run Migrations and Seeders:

* `php artisan migrate --seed`

###### Start Local Development Server:

* `php artisan serve`

###### Access the Application:

* Open a web browser and navigate to `http://localhost:8000`. Your Laravel application should be up and running.

###### Notes
* Make sure you have PHP, Composer, and a database (e.g., MySQL) installed on your local machine.
The steps above assume that your .env file is set up correctly with database credentials and other settings.