<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('home');
})->name('home');

// customer Registration
Route::controller(\App\Http\Controllers\CustomerController::class)
    ->prefix('')
    ->name('customer.')
    ->group(function () {
        Route::get('/register', 'show')->name('register');
        Route::post('/register', 'store');
    });

Route::controller(\App\Http\Controllers\AdminController::class)
    ->prefix('admin')
    ->name('admin.')
    ->group(function () {

        // admin auth routes
        Route::middleware(['guest'])
            ->group(function () {
                Route::get('login', 'login')->name('login');
                Route::post('login', 'auth');
                Route::post('logout', 'logout')
                    ->name('logout')
                    ->withoutMiddleware(['guest'])
                    ->middleware(['admin']);
            });

        // admin customize customer
        Route::middleware(['admin'])
            ->prefix('customer')
            ->name('customer.')
            ->group(function () {
                Route::get('', 'index')->name('index');
                Route::get('update/{id}', 'edit')->name('update');
                Route::put('update/{id}', 'update');
                Route::delete('destroy/{id}', 'destroy')->name('destroy');
            });
    });
