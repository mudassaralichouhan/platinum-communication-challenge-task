<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerStoreRequest;
use App\Models\Customer;
use Illuminate\Support\Facades\Redirect;

class CustomerController extends Controller
{
    public function show()
    {
        return view('customer.register');
    }

    public function store(CustomerStoreRequest $request)
    {
        Customer::create($request->all());
        return Redirect::route('home');
    }

}
