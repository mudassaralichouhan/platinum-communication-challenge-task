<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }

    public function auth(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials))
            return redirect()->intended('/admin/customer');

        else
            return Redirect::back()->withErrors(['message' => 'Invalid credentials']);
    }

    public function logout()
    {
        Auth::logout();

        return Redirect::route('home');
    }

    public function index()
    {
        $customers = Customer::all();
        return view('admin.customer.index', compact('customers'));
    }

    public function edit(Request $request, $id)
    {
        $customer = Customer::find($id);
        return view('admin.customer.update', compact(['customer']));
    }

    public function update(Request $request, $id)
    {
        Customer::where(['id' => $id])->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return Redirect::route('admin.customer.index')->withErrors(['message' => 'Update Successfully']);
    }

    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete(); // Perform the soft delete

        return Redirect::route('admin.customer.index')
            ->withErrors(['message' => 'Delete Successfully']);
    }

}
