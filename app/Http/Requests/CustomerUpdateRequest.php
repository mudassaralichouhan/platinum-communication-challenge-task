<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class CustomerUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return false;
    }

    public function rules(): array
    {
        $customerId = Route::current()->parameter('id');

        return [
            'name' => 'required|min:3|max:255',
            'email' => [
                'required', 'email', 'max:255',
                Rule::unique('customers')->ignore($customerId),
            ],
        ];
    }
}
