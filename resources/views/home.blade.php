@extends('layouts.guest')

@section('content')
  <section class="vh-100" style="background-color: #eee;">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-12 col-xl-10">
          <div class="card">
            <div class="card-header p-3">
              <div class="d-flex justify-content-between align-items-center">
                <h5 class="mb-0"><i class="fas fa-home me-2"></i>Home Page</h5>

                @if(Auth::check())
                  <form action="{{ route('admin.logout') }}" method="post" class="d-inline">
                    @csrf
                    <button type="submit" class="btn btn-danger ms-1">LOGOUT</button>
                  </form>
                @endif
              </div>
            </div>

            <div class="card-body" data-mdb-perfect-scrollbar="true" style="position: relative; height: 400px">
              <div class="row mb-4">
                <div class="col">
                  <a href="{{ route('customer.register') }}">Customer Registration</a>
                </div>
              </div>

              @if(!Auth::check())
                <div class="row mb-4">
                  <div class="col">
                    <a href="{{ route('admin.login') }}">Admin Login</a>
                  </div>
                </div>
              @else
                <div class="row mb-4">
                  <div class="col">
                    <a href="{{ route('admin.customer.index') }}">Customer Management</a>
                  </div>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection