@extends('layouts.admin')

@section('content')
  <section class="vh-100" style="background-color: #eee;">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-12 col-xl-10">
          <div class="card">
            <div class="card-header p-3">
              <h5 class="mb-0"><a href="/"><i class="fas fa-home me-2"></i></a>Admin Login</h5>
            </div>
            <div class="card-body d-flex justify-content-center align-items-center">
              <form class="col-5" action="{{ route('admin.login') }}" method="post">
                @csrf
                <div class="d-flex flex-row align-items-center mb-4">
                  <div class="flex-fill mb-0">
                    <div class="list-group list-group-light">
                      @foreach($errors->all() as $error)
                        <a href="#" class="list-group-item list-group-item-action px-3 border-0 rounded-3 mb-2 list-group-item-danger">
                          {{ $error }}
                        </a>
                      @endforeach
                    </div>
                  </div>
                </div>
                <div class="form-outline mb-4">
                  <input type="email" id="form2Example1" class="form-control" name="email"/>
                  <label class="form-label" for="form2Example1">Email address</label>
                </div>

                <div class="form-outline mb-4">
                  <input type="password" id="form2Example2" class="form-control" name="password"/>
                  <label class="form-label" for="form2Example2">Password</label>
                </div>

                <div class="row mb-4">
                  <div class="col d-flex justify-content-center">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked/>
                      <label class="form-check-label" for="form2Example31"> Remember me </label>
                    </div>
                  </div>
                </div>

                <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection