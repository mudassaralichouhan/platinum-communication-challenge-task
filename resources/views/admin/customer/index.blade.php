@extends('layouts.admin')

@section('content')
  <section class="vh-100" style="background-color: #eee;">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-12">
          <div class="card rounded-3">
            <div class="card-body p-4">

              <h5 class="mb-0"><a href="/"><i class="fas fa-home me-2"></i></a>List Customers</h5>


              <div style="max-height: 400px; overflow-y: auto;">
                <div class="d-flex flex-row align-items-center mb-4">
                  <div class="flex-fill mb-0">
                    <div class="list-group list-group-light">
                      @foreach($errors->all() as $error)
                        <a href="#" class="list-group-item list-group-item-action px-3 border-0 rounded-3 mb-2 list-group-item-secondary">
                          {{ $error }}
                        </a>
                      @endforeach
                    </div>
                  </div>
                </div>

                <table class="table mb-4">
                  <thead>
                  <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($customers as $customer)
                    <tr>
                      <th scope="row">{{ $customer->id }}</th>
                      <td>{{ $customer->name }}</td>
                      <td>{{ $customer->email }}</td>
                      <td>
                        <form action="{{ route('admin.customer.destroy', $customer->id) }}" method="post" class="d-inline">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        <a href="{{ route('admin.customer.update', $customer->id) }}" class="d-inline">
                          <button class="btn btn-success ms-1">Update</button>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
